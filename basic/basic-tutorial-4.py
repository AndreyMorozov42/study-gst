import sys
import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst


def format_ns(ns):
    s, ns = divmod(ns, 1000000000)
    m, s = divmod(s, 60)
    h, m = divmod(m, 60)
    return "%u:%02u:%02u.%09u" % (h, m, s, ns)


def handle_message(msg):
    t = msg.type
    if t == Gst.MessageType.ERROR:
        err, dbg = msg.parse_error()
        print("ERROR:", msg.src.get_name(), ":", err)
        if dbg:
            print("Debug info:", dbg)
        terminate = True
    elif t == Gst.MessageType.EOS:
        print("End-Of-Stream reached")
        terminate = True
    elif t == Gst.MessageType.DURATION_CHANGED:
        duration = Gst.CLOCK_TIME_NONE
    elif t == Gst.MessageType.STATE_CHANGED:
        old_state, new_state, pending_state = msg.parse_state_changed()
        if msg.src == playbin:
            print(f"Pipeline state changed from {Gst.Element.state_get_name(old_state)} to",
                  f"{Gst.Element.state_get_name(new_state)}")
            playing = new_state == Gst.State.PLAYING

            if playing:
                query = Gst.Query.new_seeking(Gst.Format.TIME)
                if playbin.query(query):
                    fmt, seek_enabled, start, end = query.parse_seeking()

                    if seek_enabled:
                        print(f"Seeking is ENABLED (from {format_ns(start)} to {format_ns(end)})")
                    else:
                        print("Seeking is DISABLED for this stream")
                else:
                    print("ERROR: Seeking query failed")
    else:
        print("ERROR: Unexpected message received")


playing = False
terminate = False
seek_enabled = False
seek_done = False
duration = Gst.CLOCK_TIME_NONE

Gst.init(sys.argv[1:])

playbin = Gst.ElementFactory.make("playbin", "playbin")
if not playbin:
    print("ERROR: Could not create 'playbin' element")
    sys.exit(1)

playbin.set_property("uri",
                     "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm")


ret = playbin.set_state(Gst.State.PLAYING)
if ret == Gst.StateChangeReturn.FAILURE:
    print("ERROR: Unable to set the pipeline to the playing state")
    sys.exit(1)


try:
    bus = playbin.get_bus()
    while True:
        msg = bus.timed_pop_filtered(
            100 * Gst.MSECOND,
            (Gst.MessageType.STATE_CHANGED | Gst.MessageType.ERROR
             | Gst.MessageType.EOS | Gst.MessageType.DURATION_CHANGED)
        )
        if msg:
            handle_message(msg)
        else:
            if playing:
                current = -1
                ret, current = playbin.query_position(Gst.Format.TIME)
                if not ret:
                    print("ERROR: Could not query current position")
                if duration == Gst.CLOCK_TIME_NONE:
                    (ret, duration) = playbin.query_duration(Gst.Format.TIME)
                    if not ret:
                        print("ERROR: Could not query stream duration")

                print(f"Position {format_ns(current)} / {format_ns(duration)}")
                if seek_enabled and not seek_done and current > 10 * Gst.SECOND:
                    print("Reached 10s, performing seek...")
                    playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
                                        30 * Gst.SECOND)
                    seek_done = True
        if terminate:
            break
finally:
    playbin.set_state(Gst.State.NULL)




