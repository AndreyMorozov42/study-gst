#!/usr/bin/env python3
import sys
import gi
import logging


def pad_added_handler(src, new_pad):
    print(f"Received new_pad {new_pad.get_name()} from {src.get_name()}")

    new_pad_caps = new_pad.get_current_caps()
    new_pad_struct = new_pad_caps.get_structure(0)
    new_pad_type = new_pad_struct.get_name()

    if new_pad_type.startswith("audio/x-raw"):
        sink_pad = convert.get_static_pad("sink")
    else:
        print(f"It has type {new_pad_type} which is not raw audio. Ignoring.")
        return

    if sink_pad.is_linked():
        print("We are already linked. Ignoring.")
        return

    ret = new_pad.link(sink_pad)
    if not ret == Gst.PadLinkReturn.OK:
        print(f"Type is {new_pad_type} but link failed")
    else:
        print("Link succeeded (type {new_pad_type})")
    return

gi.require_version("Gst", "1.0")

from gi.repository import Gst


logging.basicConfig(level=logging.DEBUG, format="[%(name)s] [%(levelname)8s] - %(message)s")
logger = logging.getLogger(__name__)

Gst.init(sys.argv[1:])

source = Gst.ElementFactory.make("uridecodebin", "source")
convert = Gst.ElementFactory.make("audioconvert", "convert")
resample = Gst.ElementFactory.make("audioresample", "resample")
sink = Gst.ElementFactory.make("autoaudiosink", "sink")

pipeline = Gst.Pipeline.new("test-pipeline")

if not pipeline or not source or not convert or not resample or not sink:
    print("Not all elements could be created")
    sys.exit(1)

pipeline.add(source, convert, resample, sink)

if not convert.link(resample):
    logger.error(f"Element audioconvert could not be linked to audioresample.")
    sys.exit(1)

if not resample.link(sink):
    logger.error(f"Element audioresample could not be linked to autoaudiosink.")
    sys.exit(1)

source.set_property("uri",
                    "https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm")
print(source.connect("pad-added", pad_added_handler), "kek")

ret = pipeline.set_state(Gst.State.PLAYING)
if ret == Gst.StateChangeReturn.FAILURE:
    logger.error("Unable to set the pipeline to the playing state.")
    sys.exit(1)

bus = pipeline.get_bus()
terminate = False
while True:
    msg = bus.timed_pop_filtered(
        Gst.CLOCK_TIME_NONE,
        Gst.MessageType.ERROR | Gst.MessageType.EOS)

    if not msg:
        continue

    t = msg.type
    if t == Gst.MessageType.ERROR:
        err, dbg = msg.parse_error()
        print("ERROR:", msg.src.get_name(), " ", err.message)
        if dbg:
            print("debugging info:", dbg)
        terminate = True
    elif t == Gst.MessageType.EOS:
        print("End-Of-Stream reached")
        terminate = True
    elif t == Gst.MessageType.STATE_CHANGED:
        if msg.src == pipeline:
            old_state, new_state = msg.parse_state_changed()
            print(f"Pipeline state changed from {Gst.Element.state_get_name(old_state)}",
                  f" to {Gst.Element.state_get_name(new_state)}")
    else:
        # should not get here
        print("ERROR: Unexpected message received")
        break

    if terminate:
        break

pipeline.set_state(Gst.State.NULL)